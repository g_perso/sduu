﻿using UnityEngine;

/*
Copyright 2020

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS 
BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
namespace SDUU.Math
{
	public static class Utils
	{
		public static bool IsNaN(Vector3 v) => float.IsNaN(v.x) || float.IsNaN(v.y) || float.IsNaN(v.z);
		public static bool IsNaN(Quaternion q) => float.IsNaN(q.x) || float.IsNaN(q.y) || float.IsNaN(q.z) || float.IsNaN(q.w);

		public static bool IsInfinite(Vector3 v) => (float.IsInfinity(v.x) || float.IsInfinity(v.y) || float.IsInfinity(v.z));
		public static bool IsInfinite(Quaternion q) => (float.IsInfinity(q.x) || float.IsInfinity(q.y) || float.IsInfinity(q.z) || float.IsInfinity(q.w));

		public static bool SafeAssign(ref Vector3 variable, Vector3 newValue, bool logError = true)
		{
			if (!IsNaN(newValue) && !IsInfinite(newValue))
			{
				variable = newValue;
				return true;
			}
			else if (logError) Debug.LogError("Failed to assign value " + newValue);

			return false;
		}

		public static bool SafeAssign(ref Quaternion variable, Quaternion newValue, bool logError = true)
		{
			if (!IsNaN(newValue) && !IsInfinite(newValue))
			{
				variable = newValue;
				return true;
			}
			else if (logError) Debug.LogError("Failed to assign value " + newValue);

			return false;
		}
	}
}
