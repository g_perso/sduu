﻿using System;
using UnityEngine;
 
/*
Copyright 2020

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS 
BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
namespace SDUU.Math
{
	public class Pendulum : MonoBehaviour
	{
		[SerializeField] PendulumSimulation pendulumSimulation;
		[SerializeField] bool localSimulation;

		private Vector3 Position
		{
			get { return localSimulation ? transform.localPosition : transform.position; }
			set { if (localSimulation) transform.localPosition = value; else transform.position = value; }
		}

		private Quaternion Rotation
		{
			get { return localSimulation ? transform.localRotation : transform.rotation; }
			set { if (localSimulation) transform.localRotation = value; else transform.rotation = value; }
		}

		public void Start()
		{
			pendulumSimulation.InitPendulum(Position, Rotation);
		}

		public void Update()
		{
			var pos = Position;
			var rot = Rotation;
			pendulumSimulation.UpdatePendulum(ref pos, ref rot, Time.deltaTime);
			Position = pos;
			Rotation = rot;
		}
	}

	[Serializable]
	public class PendulumSimulation
	{
		[Header("Physics parameters")]
		public float dampeningExponent = 0.01f;
		public bool useInertia = true;

		[Header("Gravity")]
		public bool useDefaultGravity = true;
		public Vector3 gravityOverride = Vector3.down * 9.81f;

		[Header("Debug")]
		public bool debugDisplay = false;

		private Vector3 pendulum;
		private Vector3 pendulumVelocity;
		private Vector3 pendulumSmoothened;

		private Quaternion lastOrientation;
		private Vector3 lastOriginPosition, lastOriginVelocity;

		public Vector3 Gravity => useDefaultGravity ? Physics.gravity : gravityOverride;

		public Vector3 PendulumVelocity => this.pendulumVelocity;
		public Vector3 PendulumDirection => this.pendulum;
		public Vector3 PendulumDirectionSmoothened => this.pendulumSmoothened;


		public void InitPendulum(Vector3 position, Quaternion orientation)
		{
			this.pendulum = pendulumSmoothened = orientation * Gravity.normalized;
			this.pendulumVelocity = Vector3.zero;
			this.lastOrientation = orientation;
			this.lastOriginPosition = position;
		}

		public void AddVelocity(Vector3 velocity)
		{
			if (!(velocity.sqrMagnitude > 0))
			{
				Debug.Log("NaN add vel");
				return;
			}

			this.pendulumVelocity += velocity;
			 
		}

		public void UpdatePendulum(ref Vector3 position, ref Quaternion orientation, float time)
		{
			if (time <= 0) return;

			float dampening = Mathf.Exp(-time * this.dampeningExponent);

			// apply object rotation to pendulum
			this.pendulum = orientation * Quaternion.Inverse(this.lastOrientation) * this.pendulum;

			var oldVelocity = this.pendulumVelocity;

			// apply linear movement forces
			Vector3 velocity = (position - this.lastOriginPosition) / time;
			if (useInertia) this.pendulumVelocity -= (velocity - this.lastOriginVelocity);
			this.lastOriginVelocity = velocity;
			this.lastOriginPosition = position;

			// apply friction
			this.pendulumVelocity *= dampening;
			Vector3 frameAverageVelocity = (this.pendulumVelocity + (oldVelocity - this.pendulumVelocity) / dampening);

			// apply pendulum rotation forces
			this.pendulumVelocity += Gravity * time;
			frameAverageVelocity += Gravity * time / 2;
			float dot = Mathf.Max(0, Vector3.Dot(pendulum, pendulumVelocity));
			if (!float.IsNaN(dot)) this.pendulumVelocity -= pendulum * dot;

			// apply speed to pendulum
			Vector3 pendulumOld = pendulum;
			Vector3 movementNormal = Vector3.Cross(pendulum, frameAverageVelocity);
			this.pendulum += frameAverageVelocity * time;
			this.pendulum = pendulum.normalized;

			//this.pendulumVelocity = Vector3.Cross(pendulum, movementNormal).normalized * pendulumVelocity.magnitude;

			// apply pendulum rotation to object
			Vector3 axis = Vector3.Cross(pendulumOld, pendulum);
			float angle = Vector3.SignedAngle(pendulumOld, pendulum, axis);
			orientation = Quaternion.AngleAxis(angle, axis) * orientation;

			this.lastOrientation = orientation;

			if (!(this.pendulum.sqrMagnitude >= 0))
			{
				this.pendulum = pendulumOld;
				Debug.Log("NaN pend");

			}

			if (!(this.pendulumVelocity.sqrMagnitude >= 0))
			{
				this.pendulumVelocity = oldVelocity;
				Debug.Log("NaN velo");
			}

			float smoothing = Mathf.Exp(-time * 300);
			this.pendulumSmoothened = (1 - smoothing) * this.pendulumSmoothened + smoothing * this.pendulum;

			// debug
			if (debugDisplay)
			{
				Debug.DrawLine(position, position + pendulum, Color.green);
				Debug.DrawLine(position + pendulum, position + pendulum + pendulumVelocity, Color.red);
			}
		}
	}
}