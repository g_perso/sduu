﻿using System.Collections.Generic;
using UnityEngine;

/*
Copyright 2020

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS 
BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
namespace SDUU.Hands
{
	[DisallowMultipleComponent]
	public class HandPose : MonoBehaviour
	{
		[SerializeField] HandPose_Data data;

		public HandPose_Data HandPoseData => data;
		public List<Transform> targetsOverride;

		private Transform targetsContainer, fingersContainer, handTarget;
		private List<Transform> fingerTargets = new List<Transform>();
		private bool rightPose = true;

		public Quaternion adjustedRotationOffset { get; private set; }

		public bool FitsRightHand => data.rightHanded || data.otherHandMirroring != OtherHandMirroring.NONE;
		public bool FitsLeftHand => !data.rightHanded || data.otherHandMirroring != OtherHandMirroring.NONE;

		public void OnValidate()
		{
			if (data == null) return;
			while (targetsOverride.Count < data.fingerTransforms.Length) targetsOverride.Add(null);
			while (targetsOverride.Count > data.fingerTransforms.Length) targetsOverride.RemoveAt(targetsOverride.Count - 1);
		}

		private Quaternion OrientUsingAxis(Vector3 axisLocal, HandAnimator handAnimator)
		{
			Vector3 handDirection = this.transform.InverseTransformPoint(handAnimator.transform.position);
			Vector3 offset = data.GetOffset(true);

			handDirection = Vector3.ProjectOnPlane(handDirection, axisLocal);
			offset = Vector3.ProjectOnPlane(offset, axisLocal);

			float angle = Vector3.SignedAngle(offset, handDirection, axisLocal);

			Debug.LogWarning(this.gameObject + " : " + angle + " " + offset + " " + handDirection + " " + axisLocal);

			return Quaternion.AngleAxis(angle, axisLocal);
		}
		
		/// <summary>
		/// Fits this hand pose to the current position of the argument animator
		/// </summary>
		/// <param name="handAnimator"></param>
		/// <returns></returns>
		public bool FitPose(HandAnimator handAnimator)
		{
			if (!targetsContainer) InitContainer();

			this.rightPose = handAnimator.IsRightHand;

			adjustedRotationOffset = Quaternion.identity;

			switch (data.rotationMode)
			{
				case PoseRotation.SINGLE_AXIS_FREE:
					adjustedRotationOffset = OrientUsingAxis(data.rotationAxis, handAnimator);
					break;
			}

			Debug.Log(data.rotationMode + " " + adjustedRotationOffset);

			return true;
		}

		private void InitContainer()
		{
			adjustedRotationOffset = Quaternion.identity;

			targetsContainer = new GameObject("HandPose").transform;
			targetsContainer.SetParent(this.transform, false);
			targetsContainer.localPosition = Vector3.zero;
			targetsContainer.localRotation = Quaternion.identity;

			fingersContainer = new GameObject("Fingers").transform;
			fingersContainer.SetParent(targetsContainer, false);
			fingersContainer.localPosition = data.GetOffset(this.rightPose);
			fingersContainer.localRotation = data.GetRotationOffset(this.rightPose);

			handTarget = new GameObject("HandTarget").transform;
			handTarget.SetParent(targetsContainer, false);
			handTarget.localPosition = data.GetOffset(this.rightPose);
			handTarget.localRotation = data.GetRotationOffset(this.rightPose);
		}

		/// <summary>
		/// The transform target for the hand
		/// </summary>
		public Transform HandTargetTransform
		{
			get
			{
				if (!handTarget) InitContainer();

				handTarget.localPosition = adjustedRotationOffset * data.GetOffset(this.rightPose);
				handTarget.localRotation = data.GetRotationOffset(this.rightPose) * Quaternion.Inverse(adjustedRotationOffset); 

				return handTarget;
			}
		}

		/// <summary>
		/// Returns the transform target for the <c>i</c>th finger
		/// </summary>
		/// <param name="i">index of the finger, starting with 0</param>
		/// <returns>the transform target for the <c>i</c>th finger</returns>
		public Transform this[int i]
		{
			get
			{
				if (i >= data.fingerTransforms.Length) return null;

				if (!fingersContainer)
				{
					InitContainer();
				}
				else
				{
					fingersContainer.localPosition = adjustedRotationOffset * data.GetOffset(this.rightPose);
					fingersContainer.localRotation = adjustedRotationOffset * data.GetRotationOffset(this.rightPose);
				}

				while (fingerTargets.Count <= i && fingerTargets.Count <= data.fingerTransforms.Length)
				{
					var target = new GameObject("Finger" + fingerTargets.Count).transform;
					target.SetParent(this.targetsContainer, false);
					fingerTargets.Add(target);
				};

				if (i < targetsOverride.Count && targetsOverride[i] != null)
				{
					fingerTargets[i].position = targetsOverride[i].position;
					fingerTargets[i].rotation = targetsOverride[i].rotation;
				}
				else
				{
					fingerTargets[i].localPosition = (adjustedRotationOffset * data.GetOffset(this.rightPose)) + (adjustedRotationOffset * data.GetRotationOffset(this.rightPose)) * data.fingerTransforms[i].position;
					fingerTargets[i].localRotation = (adjustedRotationOffset * data.GetRotationOffset(this.rightPose)) * data.fingerTransforms[i].rotation; 
				}

				return fingerTargets[i];
			}
		}
	}
}