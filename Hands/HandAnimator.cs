﻿using System.Collections.Generic;
using UnityEngine;
using System;

/*
Copyright 2020

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS 
BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
namespace SDUU.Hands
{
	/// <summary>
	/// Animates a hand using a hand pose or individual finger targets
	/// </summary>
	public class HandAnimator : MonoBehaviour
	{
		[Serializable]
		public class Finger
		{
			public Transform proximalPhalanx, middlePhalanx, distalPhalanx;
			public Vector3 touchOffset;
			public Transform fingerPoseOverride;
		}

		[SerializeField] bool isRightHand;
		[SerializeField] HandPose handPose;
		[SerializeField] Transform attachedTransform;
		[SerializeField] List<Finger> fingers;
		[SerializeField] Vector3 fingerBendNormal, fingerLateralFlexionNormal, handBackNormal;
		[SerializeField] float animationSpeed = 1;
		[SerializeField] HandPose neutralPose;

		public HandPose NeutralPose => neutralPose;
		public bool IsRightHand => isRightHand;
		public Vector3 HandBackNormal => handBackNormal;

		void Start()
		{

		}

		private Vector3 MoveTowards(Finger finger, HandPose fromPose, Vector3 target, Quaternion targetRotation)
		{
			Transform targetReference = this.transform;
			//	if (fromPose && fromPose.transform.parent != this.transform) targetReference = fromPose.HandTargetTransform;

			target = target + targetRotation * finger.touchOffset;
			target = Vector3.MoveTowards(
						transform.InverseTransformPoint(finger.distalPhalanx.position),
						targetReference.InverseTransformPoint(target),
						animationSpeed * Time.deltaTime);
			target = transform.TransformPoint(target);
			return target;
		}

		/// <summary>
		/// Set an IK target for a finger. IK targets override the current hand pose. If <paramref name="target"/> is null, the finger will be animated using the current hand pose.
		/// </summary>
		/// <param name="finger">index of the finger to which the IK target will be assigned, starting with 0</param>
		/// <param name="target">the transform to be used as a position and rotation target for the finger</param>
		public void SetIKTarget(int finger, Transform target)
		{
			fingers[finger].fingerPoseOverride = target;
		}

		/// <summary>
		/// Gets an IK target override of a finger, null if no target override has been set.
		/// </summary>
		/// <param name="finger">index of the finger, starting with 0</param>
		public Transform GetIKTargetOverride(int finger)
		{
			return fingers[finger].fingerPoseOverride;
		}

		/// <summary>
		/// Computes the preferred local grab position and rotation of an object given it's hand pose
		/// </summary>
		/// <param name="object">The object's transform</param>
		/// <param name="handPose">The hand pose to use</param>
		/// <param name="position">Preferred local position of the object in hand</param>
		/// <param name="rotation">Preferred local rotation of the object in hand</param>
		public static void PreferredGrabPositionAndRotation(Transform @object, HandPose handPose, out Vector3 position, out Quaternion rotation)
		{
			rotation = Quaternion.Inverse(@object.rotation) * handPose.HandTargetTransform.rotation;
			position = -(rotation * @object.InverseTransformPoint(handPose.HandTargetTransform.position));
		}

		/// <summary>
		/// Updates the hand animation every frame
		/// </summary>
		public void Update()
		{
			float deltaExp = 1f - Mathf.Exp(-Time.deltaTime * 20);

			if (attachedTransform && attachedTransform.parent == this.transform)
			{
				Vector3 pos;
				Quaternion rot;

				PreferredGrabPositionAndRotation(attachedTransform, handPose, out pos, out rot);

				attachedTransform.localRotation = rot;
				attachedTransform.localPosition = pos;
			}

			Transform prevTarget = null;
			Vector3 prevTargetPosition = default;
			int prevTargetReusedCount = 0;

			HandPose fromPose = null;

			for (int i = 0; i < fingers.Count; i++)
			{
				var finger = fingers[i];

				Transform target;

				if (finger.fingerPoseOverride)
				{
					target = finger.fingerPoseOverride;
					fromPose = null;
				}
				else if (handPose && handPose.HandPoseData.useFlexionData)
				{
					float randMovement = UnityEngine.Random.value < Time.deltaTime * .1f ? -5 : 0;

					FingerFlexion[] ff = handPose.HandPoseData.fingerFlexions;
					var flexion = ff[Mathf.Min(i, ff.Length - 1)];

					TrigIKUtils.FingerForwardKinematics(finger.proximalPhalanx, finger.middlePhalanx, finger.distalPhalanx,
						flexion.proximalFlexion, flexion.midAndDistalFlexion + randMovement, flexion.lateralAngle, fingerBendNormal, fingerLateralFlexionNormal, deltaExp);

					continue;
				}
				else if (handPose == null)
				{
					TrigIKUtils.FingerForwardKinematics(finger.proximalPhalanx, finger.middlePhalanx, finger.distalPhalanx,
						i == 0 ? -10 : 10, 10, 0, fingerBendNormal, fingerLateralFlexionNormal, deltaExp);

					continue;
				}
				else
				{
					target = handPose?[i];
					if (target) fromPose = handPose;
				}

				if (target != null)
				{
					TrigIKUtils.FingerIK(finger.proximalPhalanx, finger.middlePhalanx, finger.distalPhalanx,
						MoveTowards(finger, fromPose, target.position, target.rotation), target.rotation, fingerBendNormal);

					prevTarget = target;
					prevTargetPosition = target.position - finger.proximalPhalanx.position;
					prevTargetReusedCount = 0;
				}
				else if (prevTarget != null)
				{
					prevTargetReusedCount++;

					TrigIKUtils.FingerIK(finger.proximalPhalanx, finger.middlePhalanx, finger.distalPhalanx,
						MoveTowards(finger, fromPose, finger.proximalPhalanx.position + prevTargetPosition, prevTarget.rotation), prevTarget.rotation, fingerBendNormal);
				}
			}
		}

		/// <summary>
		/// Attach an object with an hand pose to this hand. Manages finger animation and object offset in hand
		/// </summary>
		/// <param name="pose">The HandPose component of the object to attach to the hand</param>
		public void Attach(HandPose pose, Transform transformToAttach = null)
		{
			pose = pose ?? neutralPose;

			if (pose)
			{
				pose.FitPose(this);

				this.attachedTransform = transformToAttach ?? pose.transform;

				pose.transform.parent = this.transform;
			}

			if (this.handPose != pose)
			{
				for (int i = 0; i < this.fingers.Count; i++) fingers[i].fingerPoseOverride = null;
				this.handPose = pose;
			} 
		}

		/// <summary>
		/// Sets a pose for this hand, without attaching the pose object. Manages finger animation only, hand placement is manual
		/// </summary>
		/// <param name="pose"></param>
		public void SetPose(HandPose pose)
		{
			pose?.FitPose(this);
			this.handPose = pose ?? neutralPose;
		}

		public void OnValidate()
		{
			Autodetect();
		}

		/// <summary>
		/// Auto-detect finger transforms and bend normals
		/// </summary>
		private void Autodetect()
		{
			if (fingers.Count == 0)
			{
				for (int i = 0; i < transform.childCount; i++)
				{
					Transform child = transform.GetChild(i);
					if (child.name.ToLower().Contains("thumb")) AutodetectFinger(0, child);
					else if (child.name.ToLower().Contains("index")) AutodetectFinger(1, child);
					else if (child.name.ToLower().Contains("ring")) AutodetectFinger(2, child);
					else if (child.name.ToLower().Contains("middle")) AutodetectFinger(3, child);
					else if (child.name.ToLower().Contains("pinky") || child.name.ToLower().Contains("little")) AutodetectFinger(4, child);
				}
			}
			else
			{
				for (int i = 0; i < fingers.Count; i++)
				{
					if (fingers[i] != null && fingers[i].proximalPhalanx && !fingers[i].middlePhalanx)
						AutodetectFinger(i, fingers[i].proximalPhalanx);
				}
			}

			if (this.fingerBendNormal.magnitude > 0)
			{
				foreach (var finger in fingers)
				{
					if (finger != null && fingerBendNormal.magnitude == 0 && finger.proximalPhalanx && finger.middlePhalanx)
						fingerBendNormal = -Vector3.Cross(
							transform.InverseTransformPoint(finger.middlePhalanx.position - finger.proximalPhalanx.position),
							this.fingerBendNormal)
						.normalized;
				}
			}
		}

		/// <summary>
		/// Auto-detect finger transforms
		/// </summary>
		private void AutodetectFinger(int index, Transform proximalPhalanx)
		{
			Transform middlePhalanx = null, distalPhalanx = null;

			int maxChilds = 0;

			for (int i = 0; i < proximalPhalanx.childCount; i++)
			{
				var child = proximalPhalanx.GetChild(i);
				if (child.localPosition.sqrMagnitude > 0 && child.childCount >= maxChilds)
				{
					middlePhalanx = child;
					maxChilds = child.childCount;
				}
			}

			if (middlePhalanx == null) return;

			maxChilds = 0;

			for (int i = 0; i < middlePhalanx.childCount; i++)
			{
				var child = middlePhalanx.GetChild(i);
				if (child.localPosition.sqrMagnitude > 0 && child.childCount >= maxChilds)
				{
					distalPhalanx = child;
					maxChilds = child.childCount;
				}
			}

			Finger finger = new Finger();
			finger.proximalPhalanx = proximalPhalanx;
			finger.middlePhalanx = middlePhalanx;
			finger.distalPhalanx = distalPhalanx;

			while (this.fingers.Count <= index) this.fingers.Add(null);

			this.fingers[index] = finger;
		}

	}
}


