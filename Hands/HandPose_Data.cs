﻿using System;
using UnityEngine;

/*
Copyright 2020

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS 
BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
namespace SDUU.Hands
{
	/// <summary>
	/// Describe a finger IK target transform (position, rotation)
	/// </summary>
	[Serializable]
	public struct FingerTransform
	{
		public Vector3 position;
		public Quaternion rotation;
	}

	[Serializable]
	public struct FingerFlexion
	{
		[Range(0, 90)] public float proximalFlexion;
		[Range(0, 90)] public float midAndDistalFlexion;
		[Range(-30, 30)] public float lateralAngle;
	}

	/// <summary>
	/// Types 
	/// </summary>
	public enum OtherHandMirroring
	{
		NONE = 0, X = 1, Y = 2, Z = 4, XY = 3, YZ = 6, XZ = 5, XYZ = 7
	}

	public enum PoseRotation
	{
		NONE = 0, SINGLE_AXIS_STEPS = 1, SINGLE_AXIS_FREE = 3, FREE_3D = 2
	}

	[Serializable, CreateAssetMenu(fileName = "HandPose", menuName = "SDUU/Hand Pose", order = 1)]
	public class HandPose_Data : ScriptableObject
	{
		[SerializeField] public FingerTransform[] fingerTransforms;

		[Header("Right Hand")]
		[SerializeField] Vector3 offset;
		[SerializeField] public Vector3 rotationOffset;

		[Header("Left Hand")]
		[SerializeField] bool useLeftHandOffsets;
		[SerializeField] Vector3 offset_LeftHand;
		[SerializeField] public Vector3 rotationOffset_LeftHand;

		public Vector3 GetOffset(bool rightHand) => rightHand ? offset : 
			(useLeftHandOffsets ? offset_LeftHand : Vector3.Scale(LeftHandScale, offset));

		public Quaternion GetRotationOffset(bool rightHand) => rightHand ? Quaternion.Euler(rotationOffset) :
			(useLeftHandOffsets ? Quaternion.Euler(rotationOffset_LeftHand) : TrigIKUtils.Mirror(Quaternion.Euler(rotationOffset), this.LeftHandScale));

		[Header("Rotation")]
		[SerializeField] public PoseRotation rotationMode;
		[SerializeField] public Vector3 rotationAxis;
		[SerializeField] public float rotationStep = 360;

		[Header("Handedness")]
		[SerializeField] public bool rightHanded = true;
		[SerializeField] public OtherHandMirroring otherHandMirroring;

		public Vector3 LeftHandScale
		{
			get
			{
				Vector3 scale = Vector3.one;
				if ((otherHandMirroring & OtherHandMirroring.X) != 0) scale.x = -1;
				if ((otherHandMirroring & OtherHandMirroring.Y) != 0) scale.y = -1;
				if ((otherHandMirroring & OtherHandMirroring.Z) != 0) scale.z = -1;
				return scale;
			}
		}

		[Header("Flexion Data")]
		[SerializeField] public bool useFlexionData = false;
		[SerializeField] public FingerFlexion[] fingerFlexions;
	}
}