﻿using System;
using UnityEngine;

/*
Copyright 2020

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS 
BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
namespace SDUU.Hands
{
	public class TrigIKUtils : MonoBehaviour
	{
		const bool newMode = true;

		public Transform prox, mid, dist, target;
		public Vector3 up, bendNormal, touchOffset;

		public void Update()
		{
			FingerIK(prox, mid, dist, target, (bendNormal));
		}

		public static Quaternion Mirror(Quaternion q, Vector3 scale)
		{
			Quaternion q2;
			q2.w = q.w;
			q2.x = q.x * Mathf.Sign(scale.y * scale.z);
			q2.y = q.y * Mathf.Sign(scale.x * scale.z);
			q2.z = q.z * Mathf.Sign(scale.x * scale.y);
			return q2;
		}

		public static Quaternion AimAtHandRotation(Transform hand, Transform objectToAim, Vector3 objectForward, Vector3 targetAimDirection, Vector3? up = null)
		{
			var lookRotation = Quaternion.LookRotation(targetAimDirection, (up == null) ? Vector3.up : up.Value);
			return AimAtHandRotation(hand, objectToAim, objectForward, lookRotation);
		}

		public static Quaternion AimAtHandRotation(Transform hand, Transform objectToAim, Vector3 objectForward, Quaternion targetAimDirection)
		{
			var handOffset = Quaternion.Inverse(Quaternion.Inverse(hand.rotation) * objectToAim.transform.rotation);
			var objectAxisOffset = Quaternion.AngleAxis(Vector3.Angle(objectForward, Vector3.forward), Vector3.Cross(objectForward, Vector3.forward));

			return targetAimDirection * objectAxisOffset * handOffset;
		}

		public static void AimAtHandTransform(Transform hand, Transform objectToAim, Transform targetAimTransform,
			out Vector3 outHandPosition, out Quaternion outHandRotation)
		{
			outHandRotation = AimAtHandRotation(hand, objectToAim, Vector3.forward, targetAimTransform?.rotation ?? Quaternion.identity);
			outHandPosition = outHandRotation * -hand.InverseTransformPoint(objectToAim.position) + (targetAimTransform?.position ?? Vector3.zero);
		}

		public static void InterpolateTransform(ref Vector3 position, ref Quaternion rotation, Vector3? targetPosition = null, Quaternion? targetRotation = null, float speed = 1)
		{
			float movementPercentage = 1 - Mathf.Exp(-Time.deltaTime * speed);

			if (targetPosition != null)
			{
				float oldDistance = Vector3.Distance(position, targetPosition.Value);

				if (oldDistance > 0)
				{
					position = Vector3.MoveTowards(position, targetPosition.Value, speed * Time.deltaTime);
					//	movementPercentage = Mathf.Clamp01((oldDistance - speed * Time.deltaTime) / oldDistance);
				}
			}

			if (targetRotation != null)
			{
				rotation = Quaternion.Slerp(rotation, targetRotation.Value, movementPercentage);
			}
		}

		public static void FingerIK(Transform proximalPhalanx, Transform middlePhalanx, Transform distalPhalanx, Transform targetTransform, Vector3 bendNormal)
		{
			FingerIK(proximalPhalanx, middlePhalanx, distalPhalanx, targetTransform.position, targetTransform.rotation, bendNormal);
		}

		public static void FingerForwardKinematics(Transform proximalPhalanx, Transform middlePhalanx, Transform distalPhalanx,
			float proximalFlexion, float midAndDistalFlexion, float lateralFlexion, Vector3 bendNormalLocal, Vector3 handBackNormal, float amount = .5f)
		{
			proximalPhalanx.localRotation =
				Quaternion.Slerp
				(
					proximalPhalanx.localRotation,
					Quaternion.AngleAxis(lateralFlexion, handBackNormal) * Quaternion.AngleAxis(proximalFlexion, bendNormalLocal),
					amount
				);

			distalPhalanx.localRotation = middlePhalanx.localRotation =
				Quaternion.Slerp(
					middlePhalanx.localRotation,
					Quaternion.AngleAxis(midAndDistalFlexion, bendNormalLocal),
					amount
				);
		}

		public static void FingerIK(Transform proximalPhalanx, Transform middlePhalanx, Transform distalPhalanx, Vector3 targetPosition, Quaternion targetRotation, Vector3 bendNormal)
		{
			Vector3 toTarget = targetPosition - proximalPhalanx.position;

			float a2 = (proximalPhalanx.position - middlePhalanx.position).sqrMagnitude;
			float b2 = (distalPhalanx.position - middlePhalanx.position).sqrMagnitude;

			float a = Mathf.Sqrt(a2), b = Mathf.Sqrt(b2);

			float c = toTarget.magnitude;
			if (c > (a + b))
			{
				toTarget = toTarget / c * (a + b);
				c = a + b;
				targetPosition = proximalPhalanx.position + toTarget;
			}
			float c2 = c * c;

			float a_proj_on_c = c * a / (a + b);
			float triangleHeight = Mathf.Sqrt(a2 - a_proj_on_c * a_proj_on_c);

			float A = Mathf.Acos((b2 + c2 - a2) / (2 * b * c)) * Mathf.Rad2Deg;
			float B = Mathf.Acos((a2 + c2 - b2) / (2 * a * c)) * Mathf.Rad2Deg;
			if (float.IsNaN(A)) A = 0;
			if (float.IsNaN(B)) B = 0;
			float C = 180 - A - B;

			//	Debug.Log(proximalPhalanx + " : " + a + " " + b + " " + c + " | " + A + " " + B + " " + C + " | " + (b2 + c2 - a2) + " " + (2 * b * c));

			OrientTowards(proximalPhalanx, middlePhalanx, targetPosition);

			Quaternion baseRotation = proximalPhalanx.rotation;
			proximalPhalanx.localRotation *= Quaternion.AngleAxis(-B, bendNormal);
			middlePhalanx.localRotation = Quaternion.AngleAxis(180 - C, bendNormal);

			Vector3 fingerPlaneNormal = middlePhalanx.rotation * bendNormal;
			Vector3 targetDirection = Vector3.ProjectOnPlane(targetRotation * Vector3.forward, fingerPlaneNormal);

			distalPhalanx.localRotation = Quaternion.identity;
			Vector3 middlePhalanxDirection = Vector3.ProjectOnPlane(distalPhalanx.position - middlePhalanx.position, fingerPlaneNormal);
			Vector3 target = Vector3.ProjectOnPlane(targetRotation * Vector3.forward, fingerPlaneNormal);

			if (Vector3.Dot(middlePhalanxDirection, -target) > Vector3.Dot(middlePhalanxDirection, target)) target = -target;

			float angle = Vector3.SignedAngle(middlePhalanxDirection, target, fingerPlaneNormal);

			distalPhalanx.localRotation = Quaternion.AngleAxis(angle, bendNormal);
			distalPhalanx.localRotation = Quaternion.identity;
		}

		public static void OrientTowards(Transform parent, Transform child, Vector3 target)
		{
			parent.localRotation = Quaternion.identity;
			parent.localRotation = Quaternion.FromToRotation(parent.InverseTransformPoint(child.position).normalized, parent.InverseTransformPoint(target).normalized);

			//	parent.localRotation = Quaternion.LookRotation(target - parent.position, Vector3.up) * Quaternion.Inverse(Quaternion.LookRotation(child.position - parent.position, Vector3.up));
		}
	}
}