﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace SDUU
{
	public static class PoolManager
	{
		private class PoolInstance
		{
			internal object @object;
			internal float creationTime;
			private bool active;

			internal bool Active
			{
				get => active;
				set
				{
					if (!active && value) creationTime = Time.time;
					active = value;
					(@object as GameObject)?.SetActive(active);
				}
			}

			internal PoolInstance(object o)
			{
				this.@object = o;
				this.creationTime = Time.time;
			}
		}

		public class Pool
		{
			private string poolName;
			private object originalObject;
			private Func<PoolInstance> instancer;
			private List<PoolInstance> instances = new List<PoolInstance>();
			private Dictionary<object, PoolInstance> instanceIndex = new Dictionary<object, PoolInstance>();
			private int lastIndex;

			public float InstancesMinimumLifetime = 0;
			private Func<object, bool> InstanceActiveCheck = null;
			private Action<object> OnReturnedToPool = null;

			internal Pool(GameObject prefab)
			{
				this.originalObject = prefab;
				this.poolName = "Prefab: " + prefab.name;
				this.instancer = () => RegisterInstance(UnityEngine.Object.Instantiate(prefab));

				// SetInstanceActiveCheck<GameObject>(o => o.activeSelf);
			}

			internal Pool(Type type, Func<object> constructor)
			{
				this.originalObject = null;
				this.poolName = type.Name;
				this.instancer = () => RegisterInstance(constructor());
			}

			private PoolInstance RegisterInstance(object @object)
			{
				PoolInstance pi = new PoolInstance(@object);
				if (IsPrefab) pools[@object] = this;
				instanceIndex[@object] = pi;
				return pi;
			}

			public bool IsPrefab => this.originalObject != null;

			public void ExpandTo(int size)
			{
				while (this.instances.Count < size) NewInstance();
			}

			public void SetInstanceActiveCheck<T>(Func<T, bool> checkFunction) where T : class
			{
				InstanceActiveCheck = o => checkFunction(As<T>(o));
			}

			public void SetReturnedToPoolAction<T>(Action<T> action) where T : class
			{
				OnReturnedToPool = o => action(As<T>(o));
			}

			private PoolInstance NewInstance()
			{
				PoolInstance instance;
				instances.Add(instance = instancer());
				OnReturnedToPool?.Invoke(instance.@object);
				instance.Active = false;
				lastIndex = instances.Count - 1;
				return instance;
			}

			private bool CanBeReused(PoolInstance poolInstance)
			{
				if (!poolInstance.Active) return true;

				if (poolInstance.creationTime < Time.time - InstancesMinimumLifetime)
				{
					if (InstanceActiveCheck == null || !InstanceActiveCheck(poolInstance.@object))
					{
						return true;
					}
				}

				return false;
			}

			public void ApplyConfigToLast()
			{
				var instance = instances[lastIndex];

				OnReturnedToPool?.Invoke(instance.@object);
				instance.Active = false; 
				instance.Active = true;
			}

			private object GetInstance()
			{
				PoolInstance instance = null;

				int i = lastIndex;
				int j = 0;

				while (instance == null)
				{
					i++;
					if (i >= instances.Count) i = 0;

					if (i == lastIndex)
					{
						instance = NewInstance();
						if(instance == null) Debug.LogError("instance=null");
						break;
					}
					else if (CanBeReused(instances[i]))
					{
						instance = instances[i];
						if (instance.Active)
						{
							OnReturnedToPool?.Invoke(instance.@object);
							instance.Active = false;
						}
						lastIndex = i;
					}
					j++;
					if(j>10000)
					{
						Debug.LogError("j>10000");
						break;
					}
				}

				instance.Active = true;
				return instance.@object;
			}

			public T GetInstance<T>() where T : class
			{
				T t = As<T>(GetInstance());
				return t;
			}

			private static T As<T>(object instanceObject) where T : class
			{
				if (instanceObject is GameObject)
				{
					var gameObject = ((GameObject)instanceObject);

					if (typeof(T) == typeof(GameObject)) return (T)instanceObject;
					else return gameObject.GetComponent<T>();
				}
				else
				{
					return instanceObject as T;
				}
			}

			internal void ReturnToPool(object @object)
			{
				var instance = instanceIndex[@object];
				if (instance.Active) OnReturnedToPool?.Invoke(instance.@object);
				instance.Active = false;
			}

			public override string ToString()
			{
				int active = 0;

				foreach (var instance in instances) if (!CanBeReused(instance)) active++;

				string output = poolName + " (" + instances.Count + " instances, i: " + lastIndex + " active: " + active + ")";

				return output;
			}

			public static implicit operator bool(Pool pool) => pool != null;
		}

		public static Pool NewPool { get; private set; }

		private static Dictionary<object, Pool> pools = new Dictionary<object, Pool>();

		private static void OnNewPool()
		{
			Debug.Log("New pool created!\n" + PoolLog());
		}

		public static string PoolLog()
		{
			string log = "Pools status\n";

			HashSet<Pool> uniquePools = new HashSet<Pool>(pools.Values);

			foreach (var pool in uniquePools) log += pool + "\n";

			return log;
		}

		public static void ReturnToPool(object @object)
		{
			Pool pool;

			if (@object is GameObject && pools.TryGetValue(@object, out pool)) pool.ReturnToPool(@object);
			else if (@object is Component && pools.TryGetValue((@object as Component).gameObject, out pool)) pool.ReturnToPool(@object);
			else if (pools.TryGetValue(@object.GetType(), out pool)) pool.ReturnToPool(@object);
		}

		public static Pool GetPrefabPool(GameObject @object)
		{
			NewPool = null;
			Pool pool;

			if (!pools.TryGetValue(@object, out pool))
			{
				pools[@object] = pool = NewPool = new Pool(@object);
				OnNewPool();
			}

			return pool;
		}

		public static Pool GetClassPool<T>(Func<T> constructor)
		{
			NewPool = null;
			Pool pool;

			if (!pools.TryGetValue(typeof(T), out pool))
			{
				pools[typeof(T)] = pool = NewPool = new Pool(typeof(T), () => constructor());
				OnNewPool();
			}

			return pool;
		}

		public static GameObject GetPrefabInstance(GameObject @object) => GetPrefabPool(@object).GetInstance<GameObject>();

		public static T GetPrefabInstance<T>(T @object) where T : Component => GetPrefabPool(@object.gameObject).GetInstance<T>();

		public static T GetClassInstance<T>(Func<T> constructor) where T : class => GetClassPool(constructor).GetInstance<T>();
	}
}